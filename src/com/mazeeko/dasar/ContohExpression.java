package com.mazeeko.dasar;

public class ContohExpression {
    public static void main(String[] args) {
        // Expression adalah core component dari statement
        // yang menghasilkan single value itu disebut dengan Expression
        int value;
        value = 10;
    }
}
