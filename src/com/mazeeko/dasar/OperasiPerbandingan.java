package com.mazeeko.dasar;
public class OperasiPerbandingan {
    public static void main(String[] args){
        Integer a = 100;
        Integer b = 100;

        // Gunakan ini untuk Membandingkan type data Number
        System.out.println(a > b);
        System.out.println(a < b);
        System.out.println(a >= b);
        System.out.println(a <= b);
        System.out.println(a == b);
        System.out.println(a != b);
    }
}
