package com.mazeeko.dasar;

import java.util.Date;

/**
 * this is a comment
 * that continues
 * across lines
 */
public class HelloDate {
    // this is a one-line comment
    public static void main(String[] args) {
        System.out.println("Hello, it's: ");
        System.out.println(new Date());
    }
}
