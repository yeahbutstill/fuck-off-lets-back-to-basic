package com.mazeeko.dasar;

public class AugmentedAssigments {
    public static void main(String[] args) {
        Integer a = 10;
        Integer b = 20;
        Integer c = 30;
        Integer d = 40;
        Integer e = 50;

        // Augmented Assigments
        a += 10; // a = a+10
        b -= 10; // b = b-10
        c *= 10; // c = c*10
        d /= 10; // d = d/10
        e %= 10; // e = e%10
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);
        System.out.println(e);
    }
}
