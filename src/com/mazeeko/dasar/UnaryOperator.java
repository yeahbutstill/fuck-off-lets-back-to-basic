package com.mazeeko.dasar;

public class UnaryOperator {
    public static void main(String[] args) {
        int d = +100; // positive
        int e = -10; // negative

        d++; // d = d+1
        System.out.println(d);

        d--; // d = d-1
        System.out.println(d);

        ++d;
        System.out.println(d);

        System.out.println(!true); // boolean kebalikan
    }
}
