package com.mazeeko.oop.yeahbutstill.repo;

public interface HasBrand {
  String getBrand();
}
