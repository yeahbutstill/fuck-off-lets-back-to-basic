package com.mazeeko.oop.yeahbutstill.repo;

public interface HelloWorld {
    void sayHello();
    void sayHello(String name);
}
