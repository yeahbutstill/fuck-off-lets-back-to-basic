package com.mazeeko.oop.yeahbutstill.data;

public class Parent {
  public String name;

  public void doIt() {
    System.out.println("Do it from parent");
  }
}
