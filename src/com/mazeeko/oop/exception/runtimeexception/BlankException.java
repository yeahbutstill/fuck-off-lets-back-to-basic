package com.mazeeko.oop.exception.runtimeexception;

public class BlankException extends RuntimeException {
    public BlankException(String message) {
        super(message);
    }
}
