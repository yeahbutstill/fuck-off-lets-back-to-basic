package com.mazeeko.oop.exception.error;

public class DatabaseError extends Error {
  public DatabaseError(String message) {
    super(message);
  }
}
