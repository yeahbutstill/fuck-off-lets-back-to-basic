package com.artivisi.example.iso8583;

public abstract class SYBConstants {
    public static final String MERCHANT_TYPE = "5413";
    public static final String ACQ_ID_CODE = "03731";
    public static final String TERMINAL_ID = "00999999";
    public static final String CURRENCY_CODE = "360";
    public static final String SYB_FORMAT_CODE = "501";
    public static final String SYB_REC_ID_INQ = "0000000000";
    public static final String AMOUNT_INQ = "00000000000000000000";
    public static final String ADMIN_FEE_INQ = "2000";
    public static final String PRODUCT_CODE_TELKOM = "104111";

    public static final String MTI_REQ = "0200";
    public static final String MTI_RESP = "0200";

    public static final String PROCESSING_CODE_INQ = "380000";
    public static final String PROCESSING_CODE_PAY = "180000";
}
