package com.tij.exercises2.operator;

/**
 * operators/PrintTest.java
 * TIJ4 Chapter Operators, Exercise 1, pager 94
 * Write a program that uses the "short" and long form of print statement
 */
public class Solution1PrintTest {
    public static void main(String[] args) {
        System.out.println("Hello, from long form");
    }
}
