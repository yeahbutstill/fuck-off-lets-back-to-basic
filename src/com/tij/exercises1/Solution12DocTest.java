package com.tij.exercises1;

import java.util.Date;

/** The first Thinking in java example program
 * Display a string and today's date
 * @author yeahbutstill
 * @author Dani
 * @version 4.0
 */
public class Solution12DocTest {
    /** Entry poing to class & application
     * @param args array of string argument
     * @throws exceptions No exceptions thrown
     */
    public static void main(String[] args) {
        System.out.println("Hello, it's: ");
        System.out.println(new Date());
    }
} /* Output: (55% match)
*/
