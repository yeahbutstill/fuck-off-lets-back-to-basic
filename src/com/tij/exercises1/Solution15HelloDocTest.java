package com.tij.exercises1;

/**
 * Public class contained in file of the same name that includes main()
 */
public class Solution15HelloDocTest {
    /** main method executed by java
     */
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
}
