package com.tij.access;

// access/Collision.java
// TIJ4 Chapter Access, Exercise 2, page 217
/* Take the code fragments in this section and turn them into a program and
 * verify that collisions do occur.
 */
public class Collision {
    public static void main(String[] args) {
        //! Vector vector = new Vector(); // ambiguous collision
        java.util.Vector v2 = new java.util.Vector<>();
    }
}
