package com.tij.access;

public class OrganizedByAccess {
    public void pub1() {
        System.out.println("pub1");
    }

    public void pub2() {
        System.out.println("pub2");
    }

    public void pub3() {
        System.out.println("pub3");
    }

    private void priv1() {
        System.out.println("priv1");
    }

    private void priv2() {
        System.out.println("priv2");
    }

    private void priv3() {
        System.out.println("priv3");
    }
}
