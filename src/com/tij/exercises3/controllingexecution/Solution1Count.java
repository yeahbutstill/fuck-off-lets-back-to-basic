package com.tij.exercises3.controllingexecution;

// control/Count.java
// TIJ4 Chapter Control, Exercise 1, page 139
// Write a program that prints values from 1 to 100
public class Solution1Count {
    public static void main(String[] args) {
        for (int i = 1; i < 101; i++) System.out.println(i);
    }
}
