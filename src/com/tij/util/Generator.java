//: net/mindview/util/Generator.java
// A generic interface.
package com.tij.util;
public interface Generator<T> { T next(); } ///:~
