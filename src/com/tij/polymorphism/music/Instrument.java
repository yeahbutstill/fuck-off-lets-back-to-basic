package com.tij.polymorphism.music;

public class Instrument {
    public void play(Note n) {
        System.out.print("Instrument.play()");
    }
}
