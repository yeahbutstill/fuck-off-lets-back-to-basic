package com.tij.initializationileanup;

public enum Spiciness {
    NOT, MILD, MEDIUM, HOT, FLAMING
}
