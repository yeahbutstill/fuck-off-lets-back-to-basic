package com.tij.initializationileanup;

public class Apricot {
    void pick() {
        System.out.println("pick first");
    }

    void pit() {
        pick();
        System.out.println("pit first");
    }
}
